<?php
/**
 * Diese PHP-Seite wird immer am ende jeder Seite aufgerufen
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/
?>

<div id="footer">

	<div id="footer-sponsoren">
		Freunde und F&ouml;rderer<br><br>
		<a href="https://stura.uni-jena.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer_images/stura.uni-jena.logo.png" width="*" height="70" alt="Studierendenrat der FSU Jena" /></a>
		<!--<a href="http://stura.inside-fhjena.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer_images/stura_eafh_logo_transparent.png" width="*" height="70" alt="Studierendenrat der EAH Jena" /></a>-->
		<a href="https://www.uni-jena.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer_images/uni-jena-logo.svg.png" width="*" height="70" alt="FSU Jena" /></a>
		<a href="http://www.campusradio-jena.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer_images/campusmedium_campusradiojena.png" width="*" height="70" alt="Campusradio Jena" /></a>
		<a href="https://www.campustv-jena.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer_images/campusmedium_campustvjena.png" width="*" height="70" alt="CampusTV Jena" /></a>
		<!--<a href="http://www.unique-online.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer_images/campusmedium_unique.png" width="*" height="70" alt="unique" /></a>-->
	</div>

	<div id="footer-template_info">
		Copyright &copy; <?php echo date("Y") ?> <b><a href="<?php bloginfo('home'); ?>"><?php bloginfo('name'); ?></a>. All rights reserved.<br/>
		<?php echo get_template() ?> Theme by <a href="http://www.hoelbing.net/">Carsten Hoelbing</a></b>.<br/>
	</div>

	<div id="footer_content">
		Friedrich-Schiller-Universit&auml;t Jena - Ernst-Abbe Platz 8 - FSR-Raum 317, 07743 Jena - <br/>
		Tel. 49 3641 9-44944 - E-Mail: fsr-kowi@uni-jena.de <br/>				
		<a href="<?php bloginfo('rss2_url'); ?>" title="RSS">RSS</a>  |
		<a href="<?php bloginfo('url'); ?>/impressum" title="Impressum">Impressum</a> | 
		<a href="<?php bloginfo('url'); ?>/kontakt" title="Kontakt">Kontakt</a> | 
		<a href="<?php echo wp_login_url(); ?>" title="Login">Login</a>
	</div>

</div><!-- END  footer-->

</div><!-- END  wrapper-->

<div id="sidebar_socialLinks">
<a href="https://www.facebook.com/fsr.kowi.FSU" target="_blank" >
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fb_icon.small.png" alt="hier geht es zum Facebook Auftritt des FSR-Kowi" title="hier geht es zum Facebook Auftritt des FSR-Kowi" width="25" height="25"/>	
</a>
<div style="align:center;"><a href="https://twitter.com/fsrkowi" class="twitter-follow-button" data-show-count="false" data-lang="de">Follow - fsrKowi<br>@Twitter</a>
<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script></div>

</div><!-- end sidebar_socialLinks -->

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
			var u=(("https:" == document.location.protocol) ? "https" : "http") + "://stats.stura.uni-jena.de/";
			_paq.push(["setTrackerUrl", u+"piwik.php"]);
			_paq.push(["setSiteId", "13"]);
			var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
			g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->
<!-- Piwik Image Tracker -->
<img src="https://stats.stura.uni-jena.de/index.php?idsite=13&amp;rec=1" style="border:0" alt="" />
<!-- End Piwik -->
	
<?php wp_footer(); ?>

</body>
</html>