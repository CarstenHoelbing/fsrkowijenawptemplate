<?php
/**
 * function
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
 * */


/*
 * Menu
 * es werden 3 Menus erstellt
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'primary-menu' => 'Main-Menu' ,
			'sidebar-links-menu' => 'Sidebar-Menu-Links',
			'sidebar-custom-menu' => 'Sidebar-Menu-custom' 
		)
	);
}
// menu in WP registrieren
add_action( 'init', 'register_my_menus' );


/*
 * Sidebar
 * es werden 4 Sidebars erstellt
 * die jeweils bei verschiedenen anzeigen angezeigt werden 
 * zb: bei einem Anzeigen eines Artikel wird nur die Sidebar 'main_sidebar_single' angezeigt
 */
register_sidebar(
	array(
		'name' => 'main_sidebar',
		'id' => 'main_sidebar',
		'description' => 'main sidebar',
		'before_title' => '<div class="sidebar_header">',
		'after_title' => '</div>',
		'before_widget' => '',
		'after_widget' => '',
	)
);
register_sidebar(
	array(
		'name' => 'main_sidebar_single',
		'id' => 'main_sidebar_single',
		'description' => 'main sidebar fuer Artikelr',
		'before_title' => '<div class="sidebar_header">',
		'after_title' => '</div>',
		'before_widget' => '',
		'after_widget' => '',
	)
);
register_sidebar(
	array(
		'name' => 'main_sidebar_archiv',
		'id' => 'main_sidebar_archiv',
		'description' => 'main sidebar fuer Archiv und Suche',
		'before_title' => '<div class="sidebar_header">',
		'after_title' => '</div>',
		'before_widget' => '',
		'after_widget' => '',
	)
);
register_sidebar(
	array(
		'name' => 'sidebar_search',
		'id' => 'sidebar_search',
		'description' => 'diese Sidebar ist NUR fuer das Widget "Suchen" gedacht',
		'before_title' => '',
		'after_title' => '',
		'before_widget' => '',
		'after_widget' => '',
	)
);

if ( function_exists('register_sidebar') )
register_sidebar();

// anpassen der eingabe kommentarfelder
function my_customize_comment_form_filter($fields) {
	$fields['author'] = '<p class="comment-form-author">' . 
	                    ' <br/>' .
	                    ' <input id="author" name="author" type="text" class="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />' .
	                    ' <label for="author">' . __( 'Name*' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
	                    '</p>';
	                     
	$fields['email'] = '<p class="comment-form-email">'.
	                    ' <br/>' .
	                    ' <input id="email" name="email" type="text" class="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />' .
						' <label for="email">' . __( 'Email*' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
	                    '</p>';
	                     
	$fields['url'] = '<p class="comment-form-url">'.
	                    ' <br/>' .
	                    ' <input id="url" name="url" type="text" class="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />' .
						' <label for="url">' . __( 'Website' ) . '</label>' .
	                    '</p>';		
	return $fields;
}
add_filter('comment_form_default_fields','my_customize_comment_form_filter');

?>