<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn ein
 * Artikel angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/

?>
<?php get_header(); ?>

<!-- content ................................. -->
<div id="content">

<?php if (have_posts()) : ?>

<?php while (have_posts()) : the_post(); ?>


	<div class="entry single">

		<?php get_template_part( 'content_single', 'page' ); ?>

		<?php comments_template( '', true ); ?>

   </div>

<?php endwhile; ?>

<?php else : ?>

	<h2>Nichts gefunden</h2>
	<p>Es tut uns leid, aber Ihre Suchanfrage hat zu keinem Ergebnis gef&uuml;hrt.</p>

<?php endif; ?>


</div> <!-- /content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>