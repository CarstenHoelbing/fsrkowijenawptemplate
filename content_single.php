<?php
/**
 * Content anzeige
 * wird aufgefrufen wenn ein Artikel angezeigt werden soll
 *
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<p class="info">
		<?php if ($post->comment_status == "open") ?>
   		<em class="date"><?php the_time('d.F Y') ?><!-- um <?php the_time('H:i')  ?>--></em>
   		<!--<em class="author"><?php the_author(); ?></em>-->
   		<?php edit_post_link('Edit','<span class="editlink">','</span>'); ?>
   		</p>

		<?php the_content();?>

		<div id="socialshareprivacy"></div>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<p id="filedunder">Artikel gespeichert unter: <?php the_category(','); ?></p>
	</footer><!-- .entry-meta -->

</article><!-- #post -->