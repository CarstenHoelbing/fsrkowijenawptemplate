<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn das 
 * Suchfeld angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/

?>
<?php
/**
 * searchform
 */
 
 //<label for="search_field" class="assistive-text">Suchen</label>
?>
<div id="search_form">
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" id="searchform">
		<fieldset>
		<input type="text" class="field" name="s" id="s"  value="..." onfocus="if (this.value == '...') {this.value = '';}" onblur="if (this.value == '') {this.value = '...';}" />
		  <input type="submit" value="Suchen" id="searchbutton" name="searchbutton" />
		</fieldset>
	</form>
</div>