<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn eine 
 * Seite/Page angezeigt werden soll.
 * 
 * Hinweis: Bei Pages soll die Sidbar nicht angezeigt werden
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/
?>
<?php get_header(); ?>

<!-- content ................................. -->

	<div id="content" role="main">
	
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content_page', 'page' ); ?>

		<?php endwhile; // end of the loop. ?>
	
	</div><!-- #content -->

<?php get_footer(); ?>