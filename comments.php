<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn eine 
 * Kommentare angezeigt werden soll. (single.php)
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/
?>

<h3 id="comments-title"><?php
    printf( _n( 'ein Kommentar zu  %2$s', '%1$s Kommentare zu %2$s', get_comments_number(), 'WPTemplate' ),
    number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );
?>
</h3>

<div id="kommentare">
	<?php wp_list_comments(); ?>
</div><!-- kommentare -->

<?php 

//Kommentar anpassen
$comments_args = array(
	'id_form'           => 'commentform',
	'id_submit'         => 'submit',
	'comment_notes_after' => '', 
	'label_submit' => 'Kommentar abschicken',
	'comment_field' => 	'<p class="comment-form-comment">'.
						' <label for="comment">' . _x( 'Comment', 'noun' ) . '</label>'.
						' <br/>'.
						' <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>'.
						'</p>'
);

comment_form($comments_args); ?>