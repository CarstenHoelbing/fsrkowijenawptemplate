<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn das
 * Suchergebnis angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/

?>
<?php get_header(); ?>

<!-- content ................................. -->
<div id="content" class="archive">

<?php if (have_posts()) : ?>

	<h2>Suchergebnis f&uuml;r <em>&#8216;<?php echo $s ?>&#8217;</em></h2>

<?php while (have_posts()) : the_post(); ?>

	<?php $custom_fields = get_post_custom(); //custom fields ?>

		<?php if (isset($custom_fields["BX_post_type"]) && $custom_fields["BX_post_type"][0] == "mini") { ?>

	<hr class="low" />

	<div class="minientry">

		<p>
		<?php echo BX_remove_p($post->post_content); ?>
		<?php comments_popup_link('(0)', '(1)', '(%)', 'commentlink', ''); ?>
		<a href="<?php the_permalink(); ?>" class="permalink" title="Permalink"><?php the_time('M j, \'y') ?><!--, <?php the_time('H:i')  ?>--></a>
		<!--<em class="author"><?php the_author() ?></em>-->
   		<?php edit_post_link('Edit','<span class="editlink">','</span>'); ?>
   		</p>

	</div>

	<hr class="low" />

	<?php } else { ?>

	<div class="entry">

		<h3><a href="<?php the_permalink() ?>" title="Permalink"><?php the_title(); ?></a></h3>

		<?php ($post->post_excerpt != "")? the_excerpt() : BX_shift_down_headlines($post->post_content); ?>

		<p class="info"><?php if ($post->post_excerpt != "") { ?><a href="<?php the_permalink() ?>" class="more">mehr lesen...</a><?php } ?>
   		<?php comments_popup_link('jetzt kommentieren?', 'bisher 1 Kommentar', 'bisher % Kommentare', 'commentlink', ''); ?>
   		<em class="date"><?php the_time('d.F Y') ?><!-- at <?php the_time('H:i')  ?>--></em>
   		<!--<em class="author"><?php the_author(); ?></em>-->
   		<?php edit_post_link('Edit','<span class="editlink">','</span>'); ?>
   		</p>

	</div>

	<?php } ?>

<?php endwhile; ?>

	<p><!-- this is ugly -->
	<span class="next"><?php previous_posts_link('neuere Beitr&auml;ge') ?></span>
	<span class="previous"><?php next_posts_link('&auml;ltere Beitr&auml;ge') ?></span>
	</p>

<?php else : ?>

	<h2>Kein Ergebnis f&uuml;r <em>&#8216;<?php echo $s ?>&#8217;</em></h2>
	<p>Es tut uns leid, aber Ihre Suchanfrage hat zu keinem Ergebnis gef&uuml;hrt.</p>

<?php endif; ?>

</div> <!-- /content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>