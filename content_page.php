<?php
/**
 * Content anzeige
 * wird aufgefrufen wenn eine Seite angezeigt werden soll
 *
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<h2><?php the_title(); ?></h2>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

</article><!-- #post -->