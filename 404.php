<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn ein
 * 404 Error entstanden ist. (Datei nicht vorhanden)
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/

get_header(); ?>

<!-- content ................................. -->
<div id="content">
  <h2>Error 404 &ndash; Datei nicht gefunden!</h2>
  <p>
      Diese gew&uuml;nschte Datei befindet sich nicht oder nicht mehr auf diesem Server. Mehr
      beim naechsten Mal!
  </p>
</div>
<!-- /content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>