<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn ein 
 * Archiv angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/

get_header(); ?>

<!-- content ................................. -->
<div id="content" class="archive">

<?php if (have_posts()) : ?>

  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
  <?php /* If this is a category archive */ if (is_category()) { ?>
  <h2>Posts gespeichert unter '<?php echo single_cat_title(); ?>'</h2>

  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
  <h2>Tagesarchiv f&uuml;r den <?php the_time('d. F Y'); ?></h2>

  <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
  <h2>Monatsarchiv f&uuml;r <?php the_time('F, Y'); ?></h2>

  <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
  <h2>Jahresarchiv f&uuml;r <?php the_time('Y'); ?></h2>

  <?php /* If this is a search */ } elseif (is_search()) { ?>
  <h2>Ihr Suchergebnis</h2>

  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
  <h2>Autorenarchiv</h2>

  <?php /* If this is a paged archive */
    } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
  ?>
  <h2>Blog-Archive</h2>

<?php } ?>

<?php while (have_posts()) : the_post(); ?>

  <?php $custom_fields = get_post_custom(); ?>

  <?php
    if (isset($custom_fields["BX_post_type"])
	&& $custom_fields["BX_post_type"][0] == "mini") {
  ?>

  <hr class="low" />

  <div class="minientry">

    <p>
      <?php echo BX_remove_p($post->post_content); ?>
      <?php comments_popup_link('(0)', '(1)', '(%)', 'commentlink', ''); ?>
      <a href="<?php the_permalink(); ?>" class="permalink" title="Permalink">
	  <?php the_time('M j, \'y') ?><!--, <?php the_time('h:ia')  ?>-->
      </a>
      <!--<em class="author"><?php the_author() ?></em>-->
      <?php edit_post_link('Edit','<span class="editlink">','</span>'); ?>
    </p>

  </div>

  <hr class="low" />

  <?php } else { ?>

  <div class="entry">

    <h3><a href="<?php the_permalink() ?>" title="Permalink">
      <?php the_title(); ?>
    </a></h3>

    <?php ($post->post_excerpt != "")? the_excerpt()
	  : BX_shift_down_headlines($post->post_content); ?>

    <p class="info"><?php if ($post->post_excerpt != "") { ?>
      <a href="<?php the_permalink() ?>" class="more">mehr lesen...</a>
      <?php } ?>
      <?php comments_popup_link('jetzt kommentieren?', 'bisher 1 Kommentar',
	'bisher % Kommentare', 'commentlink', '');
      ?>
      <em class="date">
	<?php the_time('d. F Y') ?><!-- um <?php the_time('H:i')  ?>-->
      </em>
      <!--<em class="author"><?php the_author(); ?></em>-->
      <?php edit_post_link('Edit','<span class="editlink">','</span>'); ?>
    </p>

  </div>

  <?php } ?>

<?php endwhile; ?>

  <p><!-- this is ugly -->
    <span class="next">
      <?php previous_posts_link('neuere Beitr&auml;ge') ?>
    </span>
    <span class="previous">
      <?php next_posts_link('&auml;ltere Beitr&auml;ge') ?>
    </span>
  </p>

<?php else : ?>

  <h2>Nichts gefunden</h2>
  <p>
    Es tut uns leid, aber Ihre Suchanfrage hat zu keinem Ergebnis gef&uuml;hrt.
  </p>

<?php endif; ?>

</div> <!-- /content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
