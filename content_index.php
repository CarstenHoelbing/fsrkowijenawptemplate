<?php
/**
 * Content anzeige
 * wird auf der Startseite und folgend aufgefrufen
 *
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
 */

?>

<div class="entry">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header-index">
		<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
		
	<div class="entry-content-index">
		<?php the_content('mehr lesen...');?>
		<div id="socialshareprivacy"></div>
	</div><!-- .entry-content -->

	<?php endif; ?>

	<footer class="entry-meta">
		<p class="info">
			<?php comments_popup_link('jetzt kommentieren?', 'bisher 1 Kommentar', 'bisher % Kommentare', 'commentlink', ''); ?>
	   		<?php the_time('d. F Y') /*?>
	   		unter: <?php the_category(','); */?>
	   		<?php edit_post_link('Edit','<span class="editlink">','</span>'); ?>
   		</p>
	</footer><!-- .entry-meta -->

</article><!-- #post -->
</div>