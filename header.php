<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn der 
 * Header angezeigt wird.
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/

?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
 
<head>

<title><?php 
	bloginfo( 'name' ); 
	wp_title();
	global $paged ;
	$page_max = $wp_query->max_num_pages;
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( 'Seite %s von %s ', $paged, $page_max );
?></title> 

<link rel="profile" href="http://gmpg.org/xfn/11" />

<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="<?php bloginfo('name');?>" />
<meta name="keywords" lang="de" content="Akruetzel, Jena" />

<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="content-language" content="<?php bloginfo('language');?>" />
<meta http-equiv="pragma" content="no-cache" />

<?php
	remove_action('wp_head', 'wp_generator');  // Versionsnummer entfernen 
	wp_head();
?>

<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>"  media="screen" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo ('pingback_url'); ?>" />

</head> 

<body <?php body_class($class); ?>>
<div id="container"
  <?php
    if (is_page() && !is_page("archives") && !is_page("about"))
      echo " class=\"singlecol\"";
  ?>
>

<!-- header ................................. -->
<div id="header">
	<a href="<?php bloginfo('url'); ?>">
		<div id="header_img">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fsr-kowi-icon.png" alt="<?php bloginfo('name');?>" width="100" height="100"/>
		</div>
		<div id="header_text">
			<?php bloginfo('name');?>
		</div>
	</a>
</div> <!-- /header -->

<!-- navigation ................................. -->
<div id="navigation">
	<?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
</div><!-- /navigation -->

<br />
<br />

<hr class="low" />