<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn eine 
 * Sidebar angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage FsrKowiJenaWPTemplate
 * @since FsrKowiJenaWPTemplate 1.0
*/

?>
<!-- <hr class="low" /> -->

<!-- subcontent ................................. -->
<div id="subcontent">

<?php if (is_home()) 
{
	dynamic_sidebar('main_sidebar');
}
?>

<?php if (is_single()) 
{
	dynamic_sidebar('main_sidebar_single');
} 
?>

<?php if (is_page("archives") || is_archive() || is_search()) 
{
	dynamic_sidebar('main_sidebar_archiv');
}
?>


</div> <!-- /subcontent -->